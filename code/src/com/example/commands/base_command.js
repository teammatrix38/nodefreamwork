'use strict'

/**
* @namespace com.example.commands
* @auther Graymatrix soluations PVT LTD
* @version 1.0.0
*/

/**
* @class base_command
* @classdesc Application Base Controller
* @memberof com.example.commands
*/

/**
 * Export Base Controller Class
 * @memberof com.example.commands
 * @module base_command
 * @see com.example.commands.base_controller
 */

module.exports.base_command = class base_command{
	
	constructor(){
	}
};
