'use strict'

/**
* @namespace com.example.helpers
* @auther Graymatrix soluations PVT LTD
* @version 1.0.0
*/

/**
* @class base_helper
* @classdesc Application Base Model
* @memberof com.example.helpers
*/

var Joi = require('joi');

/**
 * Export Base Helper Class
 * @memberof com.example.helpers
 * @module base_helper
 * @see com.example.helpers.base_helper
 */

module.exports.base_helper = class base_helper{
	constructor(){
	}
};
