'use strict'

/**
* @class app_controller
* @extends com.example.controllers.base_controller
* @classdesc Application global app Controller
* @memberof com.example.controllers
*/

const baseController = require("./base_controller.js").base_controller;

/**
 * Export App Controller Class
 * @memberof com.example.controllers
 * @module app_controller
 * @see com.example.controllers.app_controller
 */

module.exports.app_controller = class app_controller extends baseController{
	
	constructor(){
		super();
	}
	
	/**
	* @summary Web root function
	* @public
	* @memberof com.example.controllers.app_controller
	* @function index
	* @param {request} req - User Request Object
	* @param {response} res - User Response Object
	*/
	
	index(req,res,next){
		var db =  res.locals.SQLObject;

		res.redirect("/login");
		next();
		/**
			*@summary Create Table Example
		*/
		/*db.createTable({"id":"bigint IDENTITY(1,1)","name":"varchar(200)"},["id"],"Test",function(error,success) {
			console.log(error);
			console.log(success);
			res.status(200).send("index");
			next();
		})*/

		/**
			*@summary Get Record Example
		*/
		/*db.getRecord("Test","*",[],function(error,success) {
			console.log(error);
			console.log(JSON.stringify(success));
			res.status(200).send("index");
			next();
		})*/

		/*db.save({name:"SDCSCCSCD"},"Test",function(error,success){
			console.log(error);
			console.log(success);
			res.status(200).send("index");
			next();
		})*/

		/*db.updateAll({name:"SDCSCCSCD1"},"Test",[{"id=":1}],function(error,success){
			console.log(error);
			console.log(success);
			res.status(200).send("index");
			next();
		})*/

		/*db.deleteAll("Test",[{"id=":1}],true,function(error,success){
			console.log(error);
			console.log(success);
			res.status(200).send("index");
			next();
		})*/
		/*db.deleteAll("Test",[{"id=":1}],false,function(error,success){
			console.log(error);
			console.log(success);
			res.status(200).send("index");
			next();
		})*/
		/*db.customQuery("SELECT * from Test where id = @id; select * from Test where id = @id2",{id:4,id2:3},function(error,success){
			console.log(error);
			console.log(JSON.stringify(success));
			res.status(200).send("index");
			next();
		})*/

		/*db.createTransaction(function(error,success){
			if(error){
				console.log(error);
			}else{
				db.save({name:"sdcsdcscsclamsxakl"},"Test",function(error,success){
					db.customQuery("sele * f sl",{},function(error,success){
						if(error){
							db.rollback(function(error,success){
								console.log("rollback");
								res.status(200).send("index");
								next();
							})
						}else{
							db.commit(function(error,success){
								console.log("Commit");
								res.status(200).send("index");
								next();
							})
						}
						console.log(error);
						console.log(success);
					})
					console.log(error);
					console.log(success);
				})
			}
		})*/
	}
};
