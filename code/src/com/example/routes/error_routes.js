	'use strict'

/**
* @class error_routes
* @extends com.example.routes.base_routes
* @classdesc Application global error routes
* @memberof com.example.routes
*/

const baseRoute = require("./base_routes.js").base_routes;

/**
 * Export Error Routes Class
 * @memberof com.example.routes
 * @module error_routes
 * @see com.example.routes.error_routes
 */

module.exports.error_routes = class error_routes extends baseRoute{
	
	constructor(app){
		super(app,__filename);
	}
	
	/**
	* @summary create static URLs
	* @public
	* @memberof com.example.routes.error_routes
	* @function load
	* @override
	*/
	
	load(){
		this.app.use(this.controller.forNotFor);
		this.app.use(this.controller.fiveNotNot);
	}
};
