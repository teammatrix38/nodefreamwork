'use strict'

/**
* @class app_routes
* @extends com.example.routes.base_routes
* @classdesc Application global app routes
* @memberof com.example.routes
*/

const baseRoute = require("./base_routes.js").base_routes;
const path		= require("path");
	
/**
 * Export App Routes Class
 * @memberof com.example.routes
 * @module api_routes
 * @see com.example.routes.app_routes
 */

module.exports.app_routes = class app_routes extends baseRoute{
	
	constructor(app){
		super(app,__filename);
	}
	
	/**
	* @summary create static URLs
	* @public
	* @memberof com.example.routes.app_routes
	* @function load
	* @override
	*/
	
	load(){
		this.app.get("/",this.controller.index);
	}
};
